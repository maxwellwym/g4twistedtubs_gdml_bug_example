//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: B1DetectorConstruction.cc 94307 2015-11-11 13:42:46Z gcosmo $
//
/// \file B1DetectorConstruction.cc
/// \brief Implementation of the B1DetectorConstruction class

#include "B1DetectorConstruction.hh"

#include "G4GDMLParser.hh"
#include "G4Color.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4TwistedTubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B1DetectorConstruction::B1DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B1DetectorConstruction::~B1DetectorConstruction()
{ }

//Build world from gdml file
//Please change the path to gdml
/*
G4VPhysicalVolume* B1DetectorConstruction::Construct()
{  
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
   
  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  G4GDMLParser parser;
  parser.Read("toy.gdml");
  G4VPhysicalVolume *physWorld = parser.GetWorldVolume();
  G4LogicalVolume *logicWorld = physWorld->GetLogicalVolume();
 
  //     
  // Shape 1
  //  
  G4Material* shape1_mat = nist->FindOrBuildMaterial("G4_WATER");
  G4ThreeVector pos1 = G4ThreeVector(0, 0, 0);
        
  // Conical section shape       
  G4double shape1_rmina = 10*cm;
  G4double shape1_rminb = 11*cm;
  G4double shape1_dphi  = 5.*deg;
  G4double shape1_halfz = 25*cm;
  G4double shape1_twistedangle = 90*deg;
  G4TwistedTubs* solidShape1 =    
    new G4TwistedTubs("Shape1", shape1_twistedangle, shape1_rmina, shape1_rminb, shape1_halfz, shape1_dphi);
                      
  G4LogicalVolume* logicShape1 =                         
    new G4LogicalVolume(solidShape1,         //its solid
                        shape1_mat,          //its material
                        "Shape1");           //its name
               
  logicShape1->SetVisAttributes(G4VisAttributes(1.0, G4Colour(1.0,0.0,1.0)));

  new G4PVPlacement(0,                       //no rotation
                    pos1,                    //at position
                    logicShape1,             //its logical volume
                    "Shape1",                //its name
                    logicWorld,                //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    checkOverlaps);          //overlaps checking

  //
  //always return the physical World
  //
  return physWorld;
}
*/

//Build world from scratch
G4VPhysicalVolume* B1DetectorConstruction::Construct()
{  
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();

  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //     
  // World
  //
  G4double world_sizeXY = 2*m;
  G4double world_sizeZ  = 2*m;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");

  G4Box* solidWorld =    
    new G4Box("World",                       //its name
        0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);     //its size

  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
        world_mat,           //its material
        "World");            //its name
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
        G4ThreeVector(),       //at (0,0,0)
        logicWorld,            //its logical volume
        "World",               //its name
        0,                     //its mother  volume
        false,                 //no boolean operation
        0,                     //copy number
        checkOverlaps);        //overlaps checking

  //     
  // Shape 1
  //  
  G4Material* shape1_mat = nist->FindOrBuildMaterial("G4_WATER");
  G4ThreeVector pos1 = G4ThreeVector(0, 0, 0);

  // Conical section shape       
  G4double shape1_rmina = 10*cm;
  G4double shape1_rminb = 11*cm;
  G4double shape1_dphi  = 5.*deg;
  G4double shape1_halfz = 25*cm;
  G4double shape1_twistedangle = 90*deg;
  G4TwistedTubs* solidShape1 =    
    new G4TwistedTubs("Shape1", shape1_twistedangle, shape1_rmina, shape1_rminb, shape1_halfz, shape1_dphi);

  G4LogicalVolume* logicShape1 =                         
    new G4LogicalVolume(solidShape1,         //its solid
        shape1_mat,          //its material
        "Shape1");           //its name

  logicShape1->SetVisAttributes(G4VisAttributes(1.0, G4Colour(1.0,0.0,1.0)));

  new G4PVPlacement(0,                       //no rotation
      pos1,                    //at position
      logicShape1,             //its logical volume
      "Shape1",                //its name
      logicWorld,                //its mother  volume
      false,                   //no boolean operation
      0,                       //copy number
      checkOverlaps);          //overlaps checking

  //
  //always return the physical World
  //
  return physWorld;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
